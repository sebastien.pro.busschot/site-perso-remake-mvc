<?php
namespace BWB\Framework\mvc\models;
use BWB\Framework\mvc\UserInterface;
use BWB\Framework\mvc\models\HydrateModel;
/* 
*creer avec l'objet issue de la classe CreateEntity Class 
*/


Class Account extends HydrateModel implements UserInterface{

		private $id;

		private $firstname;

		private $lastname;

		private $email;

		private $alias;

		private $birthdate;

		private $role;

		private $id_password;


/* ____________________ Getter and Setter Part ____________________ */


	public function getId (){
		return $this->id;
	}


	public function setId ($val){
		$this->id = $val;
	}


	public function getFirstname (){
		return $this->firstname;
	}


	public function setFirstname ($val){
		$this->firstname = $val;
	}


	public function getLastname (){
		return $this->lastname;
	}


	public function setLastname ($val){
		$this->lastname = $val;
	}


	public function getEmail (){
		return $this->email;
	}


	public function setEmail ($val){
		$this->email = $val;
	}


	public function getAlias (){
		return $this->alias;
	}


	public function setAlias ($val){
		$this->alias = $val;
	}


	public function getBirthdate (){
		return $this->birthdate;
	}


	public function setBirthdate ($val){
		$this->birthdate = $val;
	}


	public function getRole (){
		return $this->role;
	}


	public function setRole ($val){
		$this->role = $val;
	}


	public function getId_password(){
		return $this->id_password;
	}


	public function setId_password ($val){
		$this->id_password = $val;
	}

	public function getPassword() {
    }

    public function getRoles() {
        return [
            $this->role
        ];
    }

    public function getUsername() {
        return $this->alias;
    }

}