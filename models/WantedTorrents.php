<?php
namespace BWB\Framework\mvc\models;
use BWB\Framework\mvc\models\HydrateModel;
/* 
*creer avec l'objet issue de la classe CreateEntity Class 
*/


Class WantedTorrents extends HydrateModel {

    private $id;

    private $name;

    private $url;

	private $validate;
	
	private $account_id;
        
/* ____________________ Getter and Setter Part ____________________ */


	public function getId (){
		return $this->id;
	}


	public function setId ($val){
		$this->id = $val;
	}


	public function getName (){
		return $this->name;
	}


	public function setName ($val){
		$this->name = $val;
	}


	public function getUrl (){
		return $this->url;
	}


	public function setUrl ($val){
		$this->url = $val;
	}


	public function getValidate (){
		return $this->validate;
	}


	public function setValidate ($val){
		$this->validate = $val;
	}

	public function getAccount_id(){
		return $this->account_id;
	}

	public function setAccount_id($val){
		$this->account_id = $val;
	}

}