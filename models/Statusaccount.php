<?php
namespace BWB\Framework\mvc\models;
use BWB\Framework\mvc\models\HydrateModel;

/* 
*creer avec l'objet issue de la classe CreateEntity Class 
*/


Class Statusaccount extends HydrateModel {

		private $account_id;

		private $status;

		private $keyActivation;


/* ____________________ Getter and Setter Part ____________________ */


	public function getAccount_id (){
		return $this->account_id;
	}


	public function setAccount_id ($val){
		$this->account_id = $val;
	}


	public function getStatus (){
		return $this->status;
	}


	public function setStatus ($val){
		$this->status = $val;
	}

	public function getKeyActivation(){
		return $this->keyActivation;
	}

	public function setKeyActivation ($val){
		$this->keyActivation = $val;
	}
	

}