<?php
namespace BWB\Framework\mvc\models;
use BWB\Framework\mvc\models\HydrateModel;

Class IpPub extends HydrateModel{
    private $ip;

    public function getIp(){
        return $this->ip;
    }

    public function setIp($ip){
        $this->ip = $ip;
    }
}