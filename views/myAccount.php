<?php
    if($_SESSION['role'] === "invite"){header("location: http://busschot-developpement.bwb") ;}
    include("views/include/header.php");
    ?>

<?php
 //var_dump($accountEntity,$passwordEntity);
?>

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Veuillez ressaisir le mot de passe</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="passwordVerify">
            <input type="hidden" name="id" value="<?php echo $passwordEntity->getId() ;?>">
            <input class="col-12" name="password" type="password" class="form-control" id="formGroupExampleInput">
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button id="buttonValidate" type="button" class="btn btn-primary"  data-target="#exampleModalCenter" data-dismiss="modal" data-toggle="modal">Valider</button>
      </div>
    </div>
  </div>
</div>

<div class="container">

    <div class="card row">
        <form class="accountForm col-12 row">
            <img class="card-img-top  col-12 col-md-4" id="logo" src="https://www.icone-png.com/png/28/28292.png" alt="Card image cap">
            <div class="card-body  col-12 col-md-8 row">
                <div class="form-group col-12 col-md-6 row">
                    <label class="col-12" for="formGroupExampleInput">Nom</label>
                    <input class="col-12" name="lastname" type="text" class="form-control" id="formGroupExampleInput" value="<?php echo $accountEntity->getLastname() ;?>">
                </div>
                <div class="form-group col-12 col-md-6 row">
                    <label class="col-12" for="formGroupExampleInput">Mot de passe</label>
                    <input class="col-12 passwordInput1" name="password" type="password" class="form-control" id="formGroupExampleInput" value="<?php echo $passwordEntity->getPassword() ;?>">
                </div>
                <div class="form-group col-12 col-md-6 row">
                    <label class="col-12" for="formGroupExampleInput">Email</label>
                    <input class="col-12"  name="email" type="email" class="form-control" id="formGroupExampleInput" value="<?php echo $accountEntity->getEmail() ;?>">
                </div>
                <div class="form-group col-12 col-md-6 row">
                    <label for="staticEmail" class="col-12">Alias</label>
                    <input type="text"  name="alias" readonly class="col-12" id="staticEmail" value="<?php echo $accountEntity->getAlias() ;?>">
                </div>
                <div class="form-group col-12 col-md-6 row">
                    <label for="staticEmail" class="col-12">Prénom</label>
                    <input type="text" name="firstname" readonly class="col-12" id="staticEmail" value="<?php echo $accountEntity->getFirstname() ;?>">
                </div>
                <div class="form-group col-12 col-md-6 row">
                    <label for="staticEmail" class="col-12">Anniversaire</label>
                    <input type="date" name="birthdate" readonly class="col-12" id="staticEmail" value="<?php echo $accountEntity->getBirthdate() ;?>">
                </div>
                <input type="hidden" name="id" value="<?php echo $accountEntity->getId() ;?>">
                <input type="hidden" name="role" value="<?php echo $accountEntity->getRole() ;?>">
                <input type="hidden" name="id_password" value="<?php echo $accountEntity->getId_password() ;?>">
                <input type="hidden" name="status" value="activate">
            </div>
        </form>
        <button class="btn btn-primary col-12" data-target="#exampleModalCenter" data-dismiss="modal" data-toggle="modal">Mettre à jour</button>
        <div class="message textred"></div>
    </div>
</div>

<?php
    //var_dump($_COOKIE);
    include("views/include/footer.php");
?>