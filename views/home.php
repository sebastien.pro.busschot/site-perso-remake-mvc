<?php include("views/include/header.php"); ?>

<h1 class="title">Bienvenue sur mon bac à sable !</h1>
<h2 class="sub-title">Une petite présentation s'impose</h2>
<img src="https://www.police.be/5269/sites/5269/files/photos/travaux.jpg" alt="entravaux">
<h2 style="text-align: center; position: relative; bottom: 100px; font-weight: bold; font-size: 300%">CSS en approche...</h2>
<div class="container">
    <div class="row">
        <!-- <img id="cv" src="assets/picture/cv2019.png" alt="cv" class="img-fluid"> -->
        <section class="infos col-12 col-md-4">
            <figure class="face">
                <img src="assets/picture/seb.png" alt="ma tête">
                <figcaption>Image temporaire</figcaption>
            </figure>
            <h3>Busschot Sébastien</h3>
            <h4>DÉVELOPPEUR JUNIOR</h4>
            <hr>
            <h4>MON PROFIL...</h4>
            <p>Curieux et autodidacte je suis régulièrement à la recherche d'une nouvelle compétence à acquérir.<br><br>
                Adepte de l'innovation et de la technologie, l'informatique m'a toujours passionné.
                J'ai la réputation du geek touche à tout mais je n'ai jamais eu l'occasion de me lancer dans la programmation, aujourd'hui c'est un rêve que je réalise !!
            </p>
            <h4>MES FORCES</h4>
            <ul class="qualitys">
                <li>Bonne gestion de groupe</li>
                <li>Envie permanente de progresser</li>
                <li>Remise en question continue</li>
                <li>Autodidacte / Autonome</li>
                <li>Dynamique</li>
            </ul>
        </section>
        <section class="experience col-12 col-md-8">
            <h3>EXPERIENCE</h3>
                <h4>Projets BeWeb</h4>
                    <h5>Exemples de projets menés en formation</h5>
                        <p>- Initiation à algorithmique en PHP, JavaScript et Ruby
                            - Mise en place d'un site généré uniquement en JavaScript
                            - Mise en service d'un serveur NodeJs, projet Netflop avec API OMDb et mise en place de l'Ajax pour les requêtes
                            - Découverte du modèle MVC, développement d'un framework PHP maison
                            - Mise au point d'une base de donnée, MCD/MPD, intégration
                            - Découverte du langage SQL et à l'utilisation de bases de données relationnelles par l'exemple
                            - Développer une application de gestion d'un centre de formation en MVC avec notre framework
                            - Initiation en CodeIgniter avec une application de ecommerce
                        </p>
                <h4>Projets personnels</h4>
                    <h5>Recueil non-exhaustif de projets fun !</h5>
                        <p>
                            Mise en route d'un serveur dédié sous Debian, Première ébauche de site web pour réunir mes TP Open Classrooms, Client léger raspberry tournant sous VNC, développer une alternative maison à NoIP et pleins d'autres petits tests... <br><br>
                            Refonte et mise en ligne de mon site, dorénavant sous busschot.fr
                        </p>
                <h4>Expérience en Stage</h4>

            <h3>DIPLOMES ET FORMATIONS</h3>
                <h4>Diplômes d'état</h4>
                    <p>BTS MUC et BAC STG Marketing</p>
                <h4>Permis</h4>
                    <p>voiture B, Bateau Côtier/Fluvial/CRR/Hauturier</p>

            <h3>SKILLS</h3>
            <div class="skills row">
                <ul class="col-6">
                    <li>JAVASCRIPT</li>
                    <li>PHP</li>
                    <li>SQL / MYSQL</li>
                    <li>REACT / ANGULAR</li>
                    <li>SYMPHONY</li>
                    <li>CODEIGNITER</li>
                    <li>NODE JS</li>
                </ul>
                <ul class="col-6">
                    <li>EXPRESS</li>
                    <li>JQUERY</li>
                    <li>BASH</li>
                    <li>KOTLIN</li>
                </ul>

            </div>

        </section>
    </div>
</div>

<?php include("views/include/footer.php");?>