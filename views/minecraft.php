<?php include("views/include/header.php"); ?>

<section id="contentMinecraft" class="container">
    <h1 class="titleOfServer">Serveur Minecraft</h1>
    <br>
    <h3>IP : 
        <span id="ipPub"></span>
    </h3>
    
    <h4>Statut: 
        <span class="statusV onLine">En ligne</span>
        <span class="statusR offLine">Hors ligne</span>
        <div class="onLine">Joueurs en ligne
            <span id="playerOnline"></span>
            /
            <span id="playerMax"></span>
        </div>
        <div class="onLine">Version du serveur: 
            <span id="serverVersion"></span>
        </div>
    </h4>
</section>

<?php include("views/include/footer.php"); ?>