<?php include("views/include/header.php"); ?>

<div class="formCenter">
    <form id="subscribePost">
        <span class="textred col" id="textHelp" class="form-text text-muted"></span>
        <div class="form-group row ">
            <div class=" col-sm-6 col-12">
                <input name="alias" type="text" class="form-control" placeholder="Identifiant">
            </div>
            <div class="col-sm-6 col-12">
                <input name="password" type="password" class="form-control " placeholder="Mot de passe">
            </div>
        </div>
        <div class="form-group row ">
            <div class="col-sm-6 col-12">
                <input name="firstname" type="text" class="form-control" placeholder="Prénom">
            </div>
            <div class="col-sm-6 col-12">
                <input name="lastname" type="text" class="form-control" placeholder="Nom de famille">
            </div>
            <div class="col-sm-12 col-12">
                <input name="email" type="email" class="form-control " placeholder="Adresse mail">
            </div>
            <div class="col-sm-12 col-12">
                <label for="birthdate"><br>Anniversaire</label>
                <input name="birthdate" type="date" class="form-control">
            </div>
        </div>
        <button class="btn btn-primary" id="subscribeButton" data-toggle="modal"
            data-target="#exampleModalCenter">S'inscrire</button>
    </form>
</div>

<div class="modal fade" id="passwordVerify" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Veuillez resaisir votre mot de passe</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="passwordVerifyPost">
                    <span class="textred col" id="textHelp" class="form-text text-muted"></span>
                    <div class="form-group row ">
                        <div class=" col-sm-12 col-12">
                            <input name="verifPassword" type="password" class="form-control" placeholder="Mot de passe">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="verifPasswordButton" class="btn btn-primary" data-target="#exampleModalCenter" data-dismiss="modal" data-toggle="modal">Valider</button>
            </div>
        </div>
    </div>
</div>

<?php
    //var_dump($_COOKIE);
    include("views/include/script.php");
?>