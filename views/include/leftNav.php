<?php
    include "assets/modules/ItemNav.php";
    $leftBar = new ItemNav;

    $sommaire = $leftBar->navLink("index","sommaire");
    $video = $leftBar->navLink("video","Médiathèque");
    $torrent = $leftBar->navLink("transmission", "Transmission");
    $minecraft = $leftBar->navLink("statusMinecraft", "Serveur Minecraft");
    $manageWanted = $leftBar->navLink("manageWanted", "Demandes Torrent");
    $arthur = $leftBar->navLink("arthur","Échographies");

    $logout = "";
    if(isset($_SESSION['role'])){
        if($_SESSION['role'] == "admin" || $_SESSION['role'] == "user"){
            $account = $leftBar->dropDown(array(
                "title" => "<i class='far fa-user-circle'></i> Mon espace",
                "Mon compte" => "my_account",
                "Se déconnecter" => "logout"
            ));
        } else {
            $account = $leftBar->navLink("subscribe", "S'inscrire");
            $logout = $leftBar->navLink("logout","Se déconnecter");
        }
    } else {
        $account = $leftBar->navLink("subscribe", "S'inscrire");
    }

    $manageUsers = $leftBar->navLink("manageUsers", "Gestion des utilisateurs");

    // $test = $leftBar->dropDown(array(
    //     "title" => "plus tard",
    //     "test" => "account"
    // ));

    $leftBar->logout = $account;
    $leftBar->admin = $account.$manageUsers.$video.$torrent.$manageWanted.$minecraft;
    $leftBar->user = $account.$video.$minecraft;
    $leftBar->invite = $account.$logout;

    echo $leftBar->switch();