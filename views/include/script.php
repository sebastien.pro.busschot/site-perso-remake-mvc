<?php ?>

<script src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
</script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
</script>
<script src="https://kit.fontawesome.com/8a4f70396d.js"></script>
<script src="assets/scripts/notifWanted.js"></script>

<?php
    switch($_SERVER['REQUEST_URI']) {
        case '/':
            echo('<script src="assets/scripts/login.js"></script>');
            break;
        case '/subscribe':
            echo('<script src="assets/scripts/subscribe.js"></script>');
            break;
        case '/forgetPassword':
            echo('<script src="assets/scripts/forget.js"></script>');
            break;
        case '/manageUsers':
            echo('<script src="assets/scripts/manageUsers.js"></script>');
            break;
        case '/my_account':
            echo('<script src="assets/scripts/myAccount.js"></script>');
            break;
        case '/video':
            echo('<script src="assets/scripts/video.js"></script>');
            break;
        case '/statusMinecraft':
            echo('<script src="assets/scripts/statusMinecraft.js"></script>');
            break;
        case '/transmission':
            echo('');
            break;
        case '/manageWanted':
            echo('<script src="assets/scripts/manageWanted.js"></script>');
            break;
        case '/arthur':
            echo('<script src="assets/scripts/arthur.js"></script>');
            break;
        default:
            $pathWhitArgs = explode("/", $_SERVER['REQUEST_URI']);
            switch (true) {
                case $pathWhitArgs[1] == "player":
                    echo('<script src="/assets/scripts/player.js"></script>');
                break;
            }

        break;
    }