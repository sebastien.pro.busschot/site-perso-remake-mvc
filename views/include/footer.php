<?php ?>

<!-- Footer -->
<footer class="page-footer font-small">

    <a class="linkFooter" href="https://mdbootstrap.com/education/bootstrap/">
        Merci
        <img class="img-fluid" src="https://miro.medium.com/max/128/0*0Uxjfi1zRM5kPGGv.png" alt="Bootstrap">
    </a>
    
    <a class="linkFooter" href="http://fablab.web-5.org/doku.php/accueil">
        A voir
        <img class="img-fluid" src="assets/picture/fablab-web-5-logo.svg" alt="Fablab">
    </a>

    <a class="linkFooter" href="https://gitlab.com/sebastien.pro.busschot/">
        Voir
        <img class="img-fluid" src="https://icon-library.net/images/gitlab-icon/gitlab-icon-19.jpg" alt="Mon Gitlab">
    </a>

    <a class="linkFooter" href="http://fabrique-beweb.com/">
        Merci
        <img class="img-fluid" src="assets/picture/BWB_logo.png" alt="BeWeb">
    </a>

</footer>
<!-- Footer -->
<?php include('script.php'); ?>

</body>

</html>