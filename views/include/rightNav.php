<?php
    include "assets/modules/ItemNav.php";
    $rightBar = new ItemNav;

    $logout = "";
    if(isset($_SESSION['role'])){
        if($_SESSION['role'] == "admin" || $_SESSION['role'] == "user"){
            $account = $rightBar->dropDown(array(
                "title" => "<i class='far fa-user-circle'></i>",
                "Mon compte" => "account",
                "Se déconnecter" => "logout"
            ));
        } else {
            $account = $rightBar->navLink("subscribe", "S'inscrire");
            $logout = $rightBar->navLink("logout","Se déconnecter");
        }
    } else {
        $account = $rightBar->navLink("subscribe", "S'inscrire");
    }
    
    $rightBar->admin = $account;
    $rightBar->user = $account;
    $rightBar->invite = $account.$logout;

    echo $rightBar->switch();