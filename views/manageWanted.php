<?php include("views/include/header.php"); ?>

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Charger le Torrent</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form enctype="multipart/form-data" action="/addTorrent" method="post" class="formAddTorrent">
          <input type="hidden" name="MAX_FILE_SIZE" value="500000" />
          <input type="hidden" name="id" value="test" class="inputId" />
          <div class="input-group mb-3">
            <div class="custom-file">
              <input name="file" type="file" class="custom-file-input" id="inputGroupFile02">
              <label class="custom-file-label" for="inputGroupFile02">Parcourir</label>
            </div>
          </div>
          <input type="submit" class="btn btn-primary sendFile" value="Envoyer">
        </form>
      </div>
    </div>
  </div>
</div>

<section class="container">
<table class="table ">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Demandeur</th>
      <th scope="col">Titre</th>
      <th scope="col">URL</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
        <tr>
          <td scope="row">#</td>
          <td>...</td>
          <td>Ajouter par le serveur</td>
          <td>...</td>
          <td><button idWanted="0" type="button" class="btn btn-primary btnModal" data-toggle="modal" data-target="#exampleModalCenter">Ajouter</button></td>
        </tr>
        <?php
          foreach ($wanted as $want) {
              //var_dump($want);
              if($want->getValidate() == "NULL"){
                ?>
                    <tr>
                        <td scope="row"><?php echo $want->getId(); ?></td>
                        <td><?php echo $want->getAccount_id(); ?></td>
                        <td><?php echo $want->getName(); ?></td>
                        <td><?php echo $want->getUrl(); ?></td>
                        <td><button idWanted="<?php echo $want->getId(); ?>" type="button" class="btn btn-primary btnModal" data-toggle="modal" data-target="#exampleModalCenter">Ajouter</button></td>
                    </tr>
                <?php
              }
          }
        ?>
  </tbody>
</table>
</section>

<?php
    //var_dump($_SESSION);
    //$_SESSION['directory'] = $_SESSION['root'];
    include("views/include/footer.php");
?>