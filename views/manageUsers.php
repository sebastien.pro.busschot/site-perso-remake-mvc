<?php
    if($_SESSION['role'] !== "admin"){header("location: http://busschot-developpement.bwb") ;}
    include("views/include/header.php");

    //var_dump($status);
    ?>
<section class="container-fluid">
    <div class="row formsDiv">
        <?php foreach ($users as $row) {
            if($row->getAlias() !== "admin"){
                ?>

                    <div class="formDiv col-12 col-sm-6 col-md-5 col-lg-4 col-xl-3 div<?php echo $row->getId()?>">
                        <form class="updateForm row">
                            <input type="text" class="form-control col-12" name="alias" value="<?php echo $row->getAlias()?>">
                            <input type="text" class="form-control col-12 " name="firstname" value="<?php echo $row->getFirstname()?>">
                            <input type="text" class="form-control col-12 " name="lastname" value="<?php echo $row->getLastname()?>">
                            <input type="email" class="form-contro col-12 " name="email" aria-describedby="emailHelp"
                                value="<?php echo $row->getEmail()?>">
                            <input class="form-control col-12 " type="date" name="birthdate" value="<?php echo $row->getBirthdate()?>">
                            <div class="row col-5">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="status" id="aStatus"
                                        name="status<?php echo $row->getId()?>" value="activate"
                                        <?php if($status[$row->getAlias()]->getStatus() == "activate"){ echo "checked"; } ?>>
                                    <label class="form-check-label" for="aStatus">Actif</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="status" id="bRadio"
                                        value="deactivate"
                                        <?php if($status[$row->getAlias()]->getStatus() != "activate"){ echo "checked"; } ?>>
                                    <label class="form-check-label" for="bRadio">Inactif</label>
                                </div>
                            </div>
                            <div class="row col-5">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" id="aRadio" name="role"
                                        value="admin" <?php if($row->getRole() == "admin"){ echo "checked"; } ?>>
                                    <label class="form-check-label" for="aRadio">Admin</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" id="bRadio" name="role"
                                        value="user" <?php if($row->getRole() == "user"){ echo "checked"; } ?>>
                                    <label class="form-check-label" for="bRadio">User</label>
                                </div>
                            </div>
                            <input type="password" name="password" class="form-control col-12 "
                                value="<?php echo ($passwords[$row->getAlias()])->getPassword() ?>">
                            <input type="hidden" name="id" value="<?php echo $row->getId()?>">
                            <input type="hidden" name="id_password" value="<?php echo $row->getId_password() ;?>">
                            <button class="btn btn-danger buttonRemove col-12 " id="<?php echo $row->getId()?>">Effacer</button>
                        </form>
                    </div>
                <?php
            }
        }?>
    </div>
    <button class="btn btn-warning" id="buttonMiseAJour">Enregistrer les modifications</button>
</section>
<?php

    //var_dump($_COOKIE);
    include("views/include/footer.php");
?>