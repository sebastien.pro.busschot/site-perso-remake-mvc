<?php include("views/include/header.php"); ?>

<div class="formCenter">
    <form id="forgetPost">
        <div class="textResponse"></div>
        <div class="form-group">
            <input name="email" type="email" class="form-control" placeholder="Votre Email">
        </div>
        <button class="btn btn-primary" id="forgetButton">M'envoyer un mail</button>
    </form>
</div>

<?php
    //var_dump($_COOKIE);
    include("views/include/script.php");
?>