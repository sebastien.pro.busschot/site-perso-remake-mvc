<?php include("views/include/header.php");?>

<div id ="container" class="container" directory="<?php echo $_SESSION['directory'] ; ?>">
    <div id="direction">
        <i class="fas fa-map-signs"></i>
        <span id="path">
            <?php
                if($_SESSION['directory'] !== $_SESSION['root']){
                    echo substr($_SESSION['directory'], 20) ;

                } else {
                    echo "Home";
                }
            ?>
        </span>
    </div>
    
    <form class="row wantNewForm">
        <input type="hidden" name="account_id" value="<?php echo $_SESSION["id"] ?>">
        <input name="name" class="form-control col-sm-12 col-md-5" type="text" placeholder="Le nom de ce que tu veux">
        <input name="url" class="form-control col-sm-12 col-md-5" type="text" placeholder="Le lien">
        <button type="button" class="wantNew btn btn-warning col-sm-12 col-md-2">Ajouter</button>
    </form>
    
    <div class="helperNewWanted"></div>

    <div>
        
        <input class="form-control researchInput" placeholder="Chercher dans la médiathèque"/>
    </div>

    <div id="directoryContainer">

    </div>

</div>

<?php
    //var_dump($_SESSION);
    //$_SESSION['directory'] = $_SESSION['root'];
    include("views/include/footer.php");
?>