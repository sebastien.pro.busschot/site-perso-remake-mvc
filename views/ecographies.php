<?php include("views/include/header.php");?>

<div id="carouselExampleFade" class="carousel slide carousel-fade container" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="assets/picture/lentille/chambre.jpg" class="d-block w-100 img-fluid" alt="cover">
    </div>
    <?php
        foreach ($pictures as $picture) {
            //var_dump($picture);
            ?>
                <div class="carousel-item">
                    <img id="<?php echo $picture["ID"] ;?>" src="<?php echo $picture["src"] ;?>" class="d-block w-100 img-fluid" alt="cover">
                </div>
            <?php
        }
    ?>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>

<?php
    if($_SESSION['role'] === "admin"){
        ?>
            <div class="row">
                <button type="button" class="btn btn-warning buttonEco col-12" data-toggle="modal" data-target="#exampleModalCenter">Ajouter une photo</button>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="fileForm" enctype="multipart/form-data" action="/arthur/upload" method="post">
                        <input type="hidden" name="MAX_FILE_SIZE" value="10000000000" />
                        <input name="picture" type="file" require/>
                        <input type="submit" class="btn btn-primary btnAddPicture" value="Ajouter l'image">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                </div>
            </div>
            </div>
        <?php
    }
    ?></div><?php
    //var_dump($_COOKIE);
    include("views/include/footer.php");
?>