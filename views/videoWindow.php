
<div class="documents">

<div class="row">
    <?php if($_SESSION["directory"] !== $_SESSION["root"]){?>
    <a href="root" class="button-folder">
        <div class="card text-center">
            <img class="card-img-top" src="assets/picture/explorer-icon.png" alt="dossier">
            <div class="card-body">
                <p class="card-text">Home</p>
            </div>
        </div>
    </a>

    <a href="preview" class="button-folder">
        <div class="card text-center">
            <img class="card-img-top" src="assets/picture/return.png" alt="dossier">
            <div class="card-body">
                <p class="card-text">Retour</p>
            </div>
        </div>
    </a>

    <div class="col-6"></div>

    <?php
        }
        $dirArray = scandir($_SESSION["directory"]);
        //var_dump($_SESSION["directory"]);
        unset($dirArray[0],$dirArray[1]);
        asort($dirArray);
        //var_dump($dirArray);

        foreach($dirArray as $row) {

            $file = $_SESSION["directory"]."/".$row;
            if(is_file($file)){

                $extension = explode(".", $row);
                $extension = $extension[count($extension) - 1];

                if($extension == "mp4"){
                    ?>
                        <a content = "<?php echo $row; ?>" class = "research" href="http://busschot.fr/player/?url=<?php echo $file; ?>">
                            <div class="card text-center card-content-loop">
                                <img class="card-img-top" src="assets/picture/video-icon.png" alt="fichier">
                                <div class="card-body">
                                    <p class="card-text"><?php  echo $row; ?></p>
                                </div>
                            </div>
                        </a>
                    <?php
                } else {
                    ?>
                        <a content = "<?php echo $row; ?>" class = "research" download="<?php echo $row; ?>" href="<?php echo $file; ?>">
                            <div class="card text-center card-content-loop">
                                <img class="card-img-top" src="assets/picture/notepad-icon.png" alt="fichier">
                                <div class="card-body">
                                    <p class="card-text"><?php  echo $row; ?></p>
                                </div>
                            </div>
                        </a>
                    <?php
                }
            } else if(is_dir($file)){
                ?>
                    <a content = "<?php echo $row; ?>" href="<?php echo $file; ?>" class="button-folder research">
                        <div class="card text-center card-content-loop">
                            <img class="card-img-top" src="assets/picture/explorer-icon.png" alt="dossier">
                            <div class="card-body">
                                <p class="card-text"><?php  echo $row; ?></p>
                            </div>
                        </div>
                    </a>
                <?php
            }
        }
        //var_dump($dirObject);

    ?>

    </div>

</div>