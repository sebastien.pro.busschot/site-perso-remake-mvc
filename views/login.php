<?php include("views/include/header.php");?>

<div class="formCenter">
    <form method="POST" id="loginPost">
        <div class="form-group">
            <span class="textred" id="textHelp" class="form-text text-muted"></span>
            <input name="alias" type="text" class="form-control" placeholder="Identifiant">
        </div>
        <div class="form-group">
            <input name="password" type="password" class="form-control" placeholder="Mot de passe">
        </div>
        <button class="btn btn-primary" id="conectionButton">Connexion</button>
        <button class="btn btn-warning"  id="inviteButton">Je suis invité</button>
        <div><a href="/forgetPassword">&#128548; J'ai oublié mon mot de passe</a></div>
        <a href='http://www.azote.org/' title='Nom de domaine' target='_blank' class="col-4">
            <img src='http://www.azote.org/pub/azote_88_31_bleu.gif ' alt='Nom de domaine' border="0" />
        </a>
    </form>
</div>

<?php
    //var_dump($_COOKIE);
    include("views/include/script.php");
?>