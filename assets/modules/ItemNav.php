<?php
    // namespace modules;
    // echo __NAMESPACE__;

    class ItemNav{

    public $admin;
    public $user;
    public $invite;
    public $logout;

    // $exemple = dropDown(array(
    //     "title" => "Menu", //! required
    //     "home" => "/"
    //     "divider" => "",
    //     "test" => "/test"
    // ));

    // $exemple = navLink('/', 'Home');

    /**
    * Select menu whit rôle
    * echo($item1.$item2[...])
    */
    public function switch(){
        if(isset($_SESSION['role'])){
            switch ($_SESSION['role']){
                case 'admin':
                return $this->admin;
                break;
                case 'user':
                return $this->user;
                break;
                case 'invite':
                return $this->invite;
                break;
                
            }
        } else {
            return $this->logout;
        }
    }


    /**
    * Create a link item
    * @href = url
    * @string = text showing
    */
    function navLink($href, $string){
        return '<li class="nav-item active">
        <a class="nav-link" href="/'.$href.'">'.$string.'</a>
        </li>';
    }

    /**
    * Create a dropdown menu item
    * @array => 
    *      required "title" for text showing
    *      you can use "divider" in key for use insert divider
    */
    function dropDown(array $items){
        
        $html = '<li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        '.$items["title"].'
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">';
        foreach ($items as $key => $value) {
            if($key === "divider"){
                $html = $html.'<div class="dropdown-divider"></div>';
            } else if($key != "title") {
                $html = $html.'<a class="dropdown-item" href="/'.$value.'">'.$key.'</a>';
            }
        }
        $html = $html.'</div></li>';
        return $html;
    }
}