$(".statusV, .statusR").hide();

$().ready(function(){

    function getIpPub(){
    $.ajax({
        type: "GET",
        url: "ip",

        success: function(res){
            let ip = JSON.parse(res);
            getStatusServer(ip.ip);
        }
    });
}

function getStatusServer(ip){
    //console.log(ip);
    $.ajax({
        url: `https://api.mcsrvstat.us/2/${ip}`,
        type: "GET",

        success: function(res, status){
            console.log(res);
            if(res["online"] == true){
                $(".onLine").show();
                $(".offLine").hide();

                $("#ipPub").text(res["ip"]);
                $("#playerOnline").text(res["players"]["online"]);
                $("#playerMax").text(res["players"]["max"]);
                $("#serverVersion").text(res["version"]);
            } else {
                $(".offLine").show();
                $(".online").hide();
            }
        },
        error: function(res, status){
            console.log(res);
        },

    });
   }

   getIpPub();
});