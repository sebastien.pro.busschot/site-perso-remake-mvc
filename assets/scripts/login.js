$(document).ready(function () {

    $.ajax({
        headers: { 
            Accept : "application/json; charset=utf-8",
            "Content-Type": "application/json; charset=utf-8"
        },
        url:"http://www.chucknorrisfacts.fr/api/get?data=param:type:txt;nb:1;tri:alea;",
        crossDomain: true,

        success: function(res){
            console.log(res);
        }
    });

    $("#conectionButton").click(function (e) {
        e.preventDefault();
        let data = $("#loginPost").serialize();
        console.log(data);

        $.ajax({
            type: "POST",
            url: "login",
            data: data,

            success: function (response, status) {
                console.log(response);

                switch (response) {
                    case "false":
                        $("#textHelp").text("Login ou mot de passe érroné !");
                        break;
                    case "true":
                        $("#textHelp").text("connecté");
                        location.reload();
                        break;
                    case "deactivate":
                        $("#textHelp").text("Votre compte est désactivé.");
                        break;
                }
            }
        });
        
    });

    $("#inviteButton").click(function (e) {
        e.preventDefault();
        $.ajax({
            type: "GET",
            url: "login",
            success: function (response, status) {
                console.log(status);
                location.reload();
            }
        });
    })
});