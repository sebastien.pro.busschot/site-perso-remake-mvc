$(document).ready(function () {

    $(".page-footer").hide();

    function isEmailAddress(str) {
        var pattern =/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return pattern.test(str);  // returns a boolean 
    }

    $("#subscribeButton").click(function(e){
        e.preventDefault();
        $('#passwordVerify').modal('show');
    });
 
    $("#verifPasswordButton").click(function (e) { 
        e.preventDefault();
        
        let data = $("#subscribePost").serialize();

        let verifPassword = $("#passwordVerifyPost").serialize();
        verifPassword = verifPassword.split("=");
        verifPassword = verifPassword[1];
        //console.log(verifPassword);
        
        let dataTab = data.split("&");

        let allIsGood = true;
        for(let i=0; i<dataTab.length; i++){
            let rowSplit = dataTab[i].split("=");
            if(rowSplit[0] == "password"){
                if(rowSplit[1] != verifPassword){
                    $("#textHelp").text("Les mot de passe ne correspondent pas");
                    allIsGood = false;
                }
            }

            if(rowSplit[0] == "email"){
                //console.log(rowSplit[1]);
                response = isEmailAddress(rowSplit[1].replace("%40", "@"));
                if(response == false){
                    $("#textHelp").text("L'addresse mail est invalide");
                    allIsGood = false;
                }
            }

            if(rowSplit[1] == ""){
                $("#textHelp").text("Merci de remplir tous les champs");
                allIsGood = false;
            }
        }

        if(allIsGood){
            //console.log(data);
            $.ajax({
                type: "POST",
                url: "addSubscribed",
                data: data,
                success: function (response, status) {
                    //console.log(status);
                    $("#textHelp").text(response);
                }
            });
        }
    });
});