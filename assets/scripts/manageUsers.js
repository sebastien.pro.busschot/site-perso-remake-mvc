$(document).ready(function () {

    $(".page-footer").hide();

    $("#buttonMiseAJour").click(function (e) { 
        e.preventDefault();
        $(".updateForm").each(function(){
            let data = $(this).serialize();
            console.log(data);

            $.ajax({
                type: "POST",
                url: "manageUsers",
                data: data,
                success: function (response) {
                    console.log(response);
                }
            });
        });
    });

    $(".buttonRemove").click(function (e) { 
        e.preventDefault();
        let id = $(this).attr('id');
        let data = "id="+id;
        //console.log(data);

        $.ajax({
            type: "PUT",
            url: "manageUsers",
            data: data,
            success: function (response) {
                //console.log(response);
                $(".div"+id).hide();
            }
        });
    });
});