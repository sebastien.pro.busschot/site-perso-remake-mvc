$().ready(function(){
    $(".nav-link").each(function(){
        if($(this).attr("href") == "manageWanted"){
            $(this).append('<span> </span><span id="badgeWanted" style="border-radius: 100%; background-color: white; color: black;" class="badge"></span>');
        }
    });

    function getNotif(){
        $.ajax({
            type: "GET",
            url: "wantedNotif",

            success: function(res){
                $("#badgeWanted").text(res);
            }
        });
    }

    getNotif();
    setInterval(function(){getNotif();}, 20000);

});