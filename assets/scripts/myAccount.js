$(document).ready(function () {

    $("#buttonValidate").click(function (e) { 
        e.preventDefault();
        
        let account = $(".accountForm").serialize();
        //console.log(account);

        let password = $("#passwordVerify").serialize();
        //console.log(password);

        //console.log(splitPassword(password));
        if(splitPassword(account) == splitPassword(password)){
            
            $.ajax({
                type: "POST",
                url: "manageUsers",
                data: account,
                success: function (response) {
                    //alert(response);
                    $('.message').text("Mis à jour");
                }
            });
        } else {
            $('.message').text("Les mot de passe ne correspondent pas");
        }

        
    });
});

function splitPassword($serialized){
    let args = $serialized.split("&");
    //console.log(args);
    for(let i = 0; args.length ; i++){
        let arg = args[i].split("=");
        if(arg[0] === "password"){
            return arg[1];
        }
    }
}