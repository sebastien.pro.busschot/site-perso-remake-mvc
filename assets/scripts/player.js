$().ready(function(){
    let parsedUrl = new URL(window.location.href);
    let url = parsedUrl.searchParams.get("url");
    let name = url.split("/");
    let link = `http://busschot.fr/${url}`;

    let html = `<video class="videoPlayer" width="70%" controls="controls">
                    <p>Votre navigateur ne gère pas les balises HTML5 video. Est'il à jour?</p>
                    <source class="sourceVideo" src="${link}" 
                        type="video/mp4" />
                        MP4 provenant de la médiathèque
                </video>`;
    $(".playerContainer").html(html);
    $("#titleMovie").text(name[1]);
    $("#downloadMovie").attr("href", link);
    $("#downloadMovie").attr("download", name[1]);

    $("embed").attr("target", link);
});