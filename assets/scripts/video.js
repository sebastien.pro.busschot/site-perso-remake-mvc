$(document).ready(function () {

    $('.wantNew').click(function(){
        let data = $('.wantNewForm').serialize();
        //console.log(data);
        
        if(isNotEmpty(data)){
            $.ajax({
                type: "POST",
                url: "/addWanted",
                data: data,
                success: function(response){
                    console.log(response);
                }
            });
            $(".helperNewWanted").text("Ajouté");
            $(".helperNewWanted").addClass("green");
            $(".helperNewWanted").removeClass("red");
        } else {
            $(".helperNewWanted").text("Merci de remplir tous les champs");
            $(".helperNewWanted").addClass("red");
            $(".helperNewWanted").removeClass("green");
        }

    });

    function isNotEmpty(data){
        let response = true;
        let arrayData = data.split("&");
        //console.log(arrayData);
        for (let i = 0; i < arrayData.length; i++) {
            let dataInput = arrayData[i].split("=");
            response = dataInput[1] == "" ? false : response;
        }
        return response;

    }
    
    function changePath(){
        $.ajax({
            type: "GET",
            url: "/path",
            dataType: "html",
            success: function (response) {
                //console.log(response);
                if(response !== "downloads_4vwNDY83a"){
                    $("#path").text(response.substring(20));
                } else {
                    $("#path").text("Home");
                }


            }
        });
    }

    function makeWidow(directory){
        //console.log(directory);

        
        $.ajax({
            type: "GET",
            url: "video/?path=" + directory,
            dataType: "html",
            success: function (response, status) {
                $("#directoryContainer").html(response);
                //console.log(status);

                $(".button-folder").on("click", function(e){
                    e.preventDefault();
                    
                    let directory = $(this).attr('href');
                    //console.log(directory);
                    makeWidow(directory);
                    changePath();
                });
            }
        });
    }

    function research(something){
        $(".research").each(function(){
            let str = $(this).attr("content").toLowerCase();
            if( str.search(something) == -1){
                $(this).hide();
            } else {
                $(this).show();
            }
        });
    }

    let directory = $(".container").attr('directory');
    makeWidow(directory);

    $(".researchInput").keyup(function(){
        research($(this).val().toLowerCase());
    });

});