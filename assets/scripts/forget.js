$(document).ready(function () {

    $(".page-footer").hide();

    $("#forgetButton").click(function(e){
        e.preventDefault();
        let datas = $("#forgetPost").serialize();
        console.log(datas);

        $.ajax({
            type: "POST",
            url: "forgetPassword/send",
            data: datas,
            success: function (response, status) {
                if(response === "Le mail vient d'être envoyé"){
                    $(".textResponse").addClass("textgreen").removeClass("textred").text(response);
                } else {
                    $(".textResponse").addClass("textred").removeClass("textgreen").text(response);
                }
                
            }
        });
    });
});