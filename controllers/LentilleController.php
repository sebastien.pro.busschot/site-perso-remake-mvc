<?php
namespace BWB\Framework\mvc\controllers;
use BWB\Framework\mvc\controllers\MY_Controller;
use BWB\Framework\mvc\dao\DAOArthur;

session_start();


class LentilleController extends MY_Controller{

    public function accueil(){
        $this->accesGuaranted($users = ['user','admin','invite']);

        $dao = new DAOArthur;
        $pictures = $dao->getAll();
        //var_dump($pictures);

        $data = array(
            "title" => "Echographies",
            "pictures" => $pictures,
        );

        $this->render("ecographies", $data);
    }

    public function upload(){
        var_dump($_FILES);

        $extension = pathinfo($_FILES["picture"]["name"], $options = PATHINFO_EXTENSION);
        if( $extension === "jpg" || $extension === "png"){
            echo "The extension is good.";

            $path = "assets/picture/lentille/".$_FILES["picture"]["name"];
            move_uploaded_file ($_FILES["picture"]["tmp_name"], $path);

            $dao = new DAOArthur;
            $dao->create($path);

        } else {
            echo "It's not an PNG or an JPG";
        }

        header("location: /arthur");
    }

    public function remove(){
        
    }


}