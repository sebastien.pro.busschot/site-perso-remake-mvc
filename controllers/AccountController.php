<?php
namespace BWB\Framework\mvc\controllers;
use BWB\Framework\mvc\controllers\MY_Controller;
use BWB\Framework\mvc\SecurityMiddleware;
use BWB\Framework\mvc\dao\DAOAccount;
use BWB\Framework\mvc\dao\DAOPassword;
use BWB\Framework\mvc\dao\DAOStatusaccount;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

session_start();

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ViewController
 *
 * @author loic
 */
class AccountController extends MY_Controller{

    public function home(){
        $this->accesGuaranted($users = ['invite','user','admin']);

        if(!isset($_SESSION['root'])){
            $_SESSION['directory'] = "downloads_4vwNDY83a";
            $_SESSION['root'] = "downloads_4vwNDY83a";
        }
    
        $datas = array(
            "title" => "Bienvenue sur busschot.fr"
        );
        $this->render('home', $datas);
    }

    public function subscribe(){
        $datas = array(
            "title" => "Inscription"
        );
        $this->render('subscribe', $datas);
    }

    public function addSubscribed(){
        // récupèrer ajax en post, puis envoyer mail de confirmation pour modifier status
        $pwd = new DAOPassword;
        $entityPsw = $pwd->getEntity($this->inputPost());
        $pwd->create($entityPsw);
        $psswdId = $pwd->lastId();

        // var_dump($entityPsw,$psswdId);

        $account = new DAOAccount;
        $count1 = $account->countAccounts();
        $entityAc = $account->getEntity($this->inputPost());
        $entityAc->setId_password($psswdId);
        $entityAc->setRole("user");
        $account->create($entityAc);
        $accountId = $account->lastId();
        $count2 = $account->countAccounts();

        // var_dump($count1,$count2);
        // var_dump($entityAc,$accountId);

        if($count1 === $count2){
            $pwd->delete($psswdId);
            echo("Erreur, essayez un autre identifiant et vérifiez que vous n'avez pas déja utilisé ce mail");
            
        } else {
            $status = new DAOStatusaccount;
            $entitySt = $status->getEntity($this->inputPost());
            $entitySt->setAccount_id($accountId);
            $entitySt->setStatus("unactivate");
            $entitySt->setKeyActivation(md5(microtime(TRUE)*100000));
            $status->create($entitySt);

            $this->sendMailValidation($entityAc,$entitySt);
        }
    }

    public function sendMailValidation($entityAc, $entitySt){
        $body = file_get_contents("assets/modules/emailDeBienvenue.html");
        $body = str_replace("#ALIASCHANGE#", $entityAc->getAlias(), $body);
        $body = str_replace("#KEYCHANGE#", $entitySt->getKeyActivation(), $body);

        $altBody = 'Bonjour et merci de vous joindre a moi,
        Cliquez sur : http://busschot.fr/activation/?alias='.$entityAc->getAlias().'&keyActivation='.$entitySt->getKeyActivation().' 
        Merci de votre confiance,
        Cordialement,
        Sébastien';

        $subject = 'Confirmation d\'inscription pour Busschot.fr';

        $datas = array(
            'addressSend' => $entityAc->getEmail(),
            'aliasSend'    => $entityAc->getAlias(),
            'bodyHTML'    => $body,
            'altBody'     => $altBody,
            'subject'     => $subject

        );

        $this->sendMail($datas);
    }

    public function forgetPassword(){
        $datas = array(
            "title" => "Recuperation mot de passe"
        );
        $this->render('forget', $datas);
    }

    public function sendForgetPassword(){
        $account = new DAOAccount;
        $entityAc = $account->getEntity($this->inputPost());
        $res = $account->getAllBy(array(
            "email" => $entityAc->getEmail()
        ));
        //var_dump($res);

        if($res == null){
            echo "Email inconnu";
            die();
        }
        $entityAc = $res[0];

        $pwd = new DAOPassword;
        $entityPsw = $pwd->retrieve($entityAc->getId_password());
        $tempPsw = $this->generatePsw();
        $entityPsw->setPassword(password_hash ( $tempPsw, PASSWORD_BCRYPT, ['cost'=>9]));
        $pwd->update($entityPsw);

        $subject = "Busschot.fr, Recuperation mot de passe";
        $body = file_get_contents("assets/modules/emailPassword.html");
        $body = str_replace("#PASSWORDCHANGE#", $tempPsw, $body);
        $altBody = "Bonjour, voici votre nouveau mot de passe : ". $tempPsw .". Changez-le ;)" ;

        $datas = array(
            'addressSend' => $entityAc->getEmail(),
            'aliasSend'    => $entityAc->getAlias(),
            'bodyHTML'    => $body,
            'altBody'     => $altBody,
            'subject'     => $subject

        );

        //var_dump($datas);

        $this->sendMail($datas);
    }

    public function sendMail(array $datas){
        $account = new DAOAccount;
        $filter = array(
            "alias" => "admin"
        );
        
        $admin = $account->getAllBy($filter);
        //var_dump($admin);
        $admin = $admin[0];
        $idPassword = $admin->getId_password();
        //var_dump($idPassword);
        $password = new DAOPassword;
        $password = $password->retrieve($idPassword);
        //var_dump($entityAc,$entitySt,$admin);
        
        $mail = new PHPMailer(true);
        //var_dump($mail);
        try {
            //Server settings
            $mail->SMTPDebug = 0;                                       // Enable verbose debug output
            $mail->isSMTP();                                            // Set mailer to use SMTP
            $mail->Host       = 'smtp.gmail.com';                       // Specify main and backup SMTP servers
            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
            $mail->Username   = $admin->getEmail();                     // SMTP username
            $mail->Password   = $password->getPassword();               // SMTP password
            $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
            $mail->Port       = 587;                                    // TCP port to connect to
            
        
            //Recipients
            $mail->setFrom($admin->getEmail(), 'Admin');
            $mail->addAddress($datas['addressSend'],$datas['aliasSend']);
        
            // // Attachments
            // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
        
            // Content
            $mail->Subject = $datas['subject'];
            $mail->Body    = $datas['bodyHTML'];
            $mail->AltBody = $datas['altBody'];
        
            //var_dump($mail);
            $mail->send();
            echo "Le mail vient d'être envoyé";
        } catch (Exception $e) {
            echo "Une erreur est survenue, contactez l'administrateur.";
        }
        
        

        //var_dump($mail);
    }

    public function activation(){
        
        $account = new DAOAccount;
        $user = $account->getAllBy(array(
            "alias" => $this->inputGet()['alias']
        ));
        $user = $user[0];
        //var_dump($user);
                
        $status = new DAOStatusaccount;
        $entityStatus = $status->getAllBy(array(
            "account_id" => $user->getId()
        ));
        $entityStatus = $entityStatus[0];
        var_dump($entityStatus);
        
        if($entityStatus->getKeyActivation() === $this->inputGet()['keyActivation'] ){
            echo("Les clés correspondent ");
            $entityStatus->setStatus('activate');
            //var_dump($entityStatus);
            
            $status->update($entityStatus);
            header('Location: http://busschot.fr/');
        }

    }

    private function generatePsw(){
        $psw = "";
        for ($i=0; $i < 10; $i++) { 
            $psw .= random_int(0, 1000);
        }
        return $psw;
    }
    
}