<?php
namespace BWB\Framework\mvc\controllers;

use BWB\Framework\mvc\controllers\MY_Controller;
use BWB\Framework\mvc\controllers\AccountController;
use BWB\Framework\mvc\dao\DAOWantedTorrents;
use BWB\Framework\mvc\dao\DAOAccount;
use BWB\Framework\mvc\models\WantedTorrents;
use BWB\Framework\mvc\models\Account;

session_start();


class VideoController extends MY_Controller{

    public function player(){
        $this->accesGuaranted($users = ["user","admin"]);
        //var_dump($this->inputGet());
        $datas = array(
            "title" => "Player Video"
        );
        $this->render("player", $datas);
    }

    public function container(){

        $this->accesGuaranted($users = ['user','admin']);
        
        $datas = array(
            "title" => "Médiathèque"
        );
        $this->render("video", $datas);
    }

    public function getPath(){
        echo $_SESSION["directory"] ;
    }

    public function refresh(){
        //var_dump($this->inputGet()['path']);
        $this->accesGuaranted($users = ['user','admin']);
        
        switch ($this->inputGet()['path']) {
            case 'root':
            $_SESSION["directory"] = $_SESSION["root"];
            break;
            case 'preview':
            if($_SESSION["directory"] !== $_SESSION["root"]){
                //var_dump($_SESSION["directory"]);
                $previewDir = explode("/",$_SESSION["directory"]);
                unset($previewDir[count($previewDir)-1]);
                $_SESSION["directory"] = implode("/", $previewDir);
                //var_dump($_SESSION["directory"]);
            }
            break;
            
            default:
                $_SESSION["directory"] = isset($this->inputGet()['path']) ? $this->inputGet()['path'] : $_SESSION["directory"] ;
            break;
        }
        //var_dump($_SESSION);
        
        
        $this->render("videoWindow");
    }

    public function showTransmission(){

        $this->accesGuaranted($users = ['admin']);

        $datas = array (
            "title" => "Transmission",
        );

        $this->render("transmission", $datas);
    }

    public function addTorrent(){
        $daoWantedTorrents = new DAOWantedTorrents;
        //var_dump($_FILES,$_POST);
        $fileName = str_replace(" ", "_", $_FILES['file']['name']);
        $extAvailable = pathinfo ( $fileName , $options = PATHINFO_EXTENSION) == "torrent" ? true : false;
        $sizeAvailable = filesize ( $_FILES['file']['tmp_name'] ) < 500000 ? true : false;
        
        if($extAvailable && $sizeAvailable){
            //var_dump($fileName);
            move_uploaded_file ( $_FILES['file']['tmp_name'] , "Torrents/".$fileName );
            //echo shell_exec('pwd');
            chdir ( "/var/www/html/busschot/downloads_4vwNDY83a/");
            //echo shell_exec('pwd');
            shell_exec("transmission-remote -n Gandalf:ami -a /var/www/html/busschot/Torrents/".$fileName);
            $daoWantedTorrents = new DAOWantedTorrents;
            $wanted = new WantedTorrents(array(
                "id" => $_POST['id'],
                "validate" => "validate",
            ));
            //var_dump($wanted);
            $daoWantedTorrents->update($wanted);

            $wanted = $daoWantedTorrents->retrieve($wanted->getId());
            $wanted = new WantedTorrents($wanted);
            //var_dump($wanted);

            $daoAccount = new DAOAccount;
            $account = $daoAccount->retrieve($wanted->getAccount_id());
            //var_dump($account);

            $this->sendMailWanted(array(
                "account" => $account,
                "wanted" => $wanted,
            ));
            
        }
        header('Location: http://busschot.fr/manageWanted');
    }

    public function wantedTorrent(){
        $this->accesGuaranted($users = ['admin']);

        $daoWantedTorrents = new DAOWantedTorrents;
        $wanted = $daoWantedTorrents->getAll();
        //var_dump($wanted);

        $daoAccount = new DAOAccount;
        
        foreach ($wanted as $key => $value) {
            $id = $wanted[$key]->getaccount_id();
            $account = $daoAccount->retrieve($id);
            //var_dump($account);
            $wanted[$key]->setaccount_id($account->getFirstname()." ".$account->getLastname());
        }

        $datas = array(
            "title"  => "Gestion des demandes de Torrent",
            "wanted" => $wanted,
        );

        $this->render('manageWanted', $datas);
    }

    public function notificationTorrent(){
        $daoWantedTorrents = new DAOWantedTorrents;
        echo ($daoWantedTorrents->count()['count']);
    }

    public function addWanted(){
        var_dump($this->inputPost());
        $daoWantedTorrents = new DAOWantedTorrents;
        $wanted = new WantedTorrents($this->inputPost());
        var_dump($wanted);
        $daoWantedTorrents->create($wanted);
    }

    public function sendMailWanted($array){

        $accountController = new AccountController;
        
        $subject = "Busschot.fr, Votre demande de contenu vient d'etre validee";
        $body = file_get_contents("../assets/modules/emailWanted.html");
        $body = str_replace("#rappel#", $array["wanted"]->getName(), $body);
        // echo($body);
        $altBody = "Bonjour, ".$array["wanted"]->getName()." vient d'être ajouté aux téléchargement. Ça devrait être dispo rapidement." ;
        
        $datas = array(
            'addressSend' => $array["account"]->getEmail(),
            'aliasSend'    => $array["account"]->getAlias(),
            'bodyHTML'    => $body,
            'altBody'     => $altBody,
            'subject'     => $subject

        );

        $accountController->sendMail($datas);
    }
}
