<?php
namespace BWB\Framework\mvc\controllers;

use BWB\Framework\mvc\Controller;
use BWB\Framework\mvc\dao\DAOAccount;
use BWB\Framework\mvc\dao\DAOStatusaccount;
use BWB\Framework\mvc\models\DefaultModel;
use BWB\Framework\mvc\dao\DAOPassword;

use Exception;
session_start();

class DefaultController extends Controller
{

    function __construct()
    {
        parent::__construct();
    }

    public function getDefault()
    {
        if($this->security->acceptConnexion() && isset($_SESSION['role'])){
            header('Location: http://busschot.fr/home');
        }

        $datas = array(
            "title" => "Login"
        );
        $this->response->render("login", $datas);
    }

    public function invite(){
        $_SESSION['role'] = "invite";
        $_SESSION['alias'] = "Invité";
        //var_dump(new DefaultModel);
        $this->security->generateToken(new DefaultModel);
    }

    public function login()
    {
        $account = new DAOAccount;
        $password = new DAOPassword;
        $account = $account->getAllBy(["alias"=>$this->inputPost()['alias']]);
        if($account != null){
            
            $account = $account[0];
            $password = $password->retrieve($account->getId_password());
            $verify = password_verify ( $this->inputPost()['password'], $password->getPassword() );
            if(!$verify){
                echo "false";
                die();
            }
            
            $daoStatus = new DAOStatusaccount;
            $statusEnity = $daoStatus->retrieve($account->getId() );

            if($statusEnity->getStatus() == "activate"){
                $_SESSION['id'] = $account->getId();
                $_SESSION['firstname'] = $account->getFirstname();
                $_SESSION['lastname'] = $account->getLastname();
                $_SESSION['email'] = $account->getEmail();
                $_SESSION['alias'] = $account->getAlias();
                $_SESSION['birthdate'] = $account->getBirthdate();
                $_SESSION['role'] = $account->getRole();
                $this->security->generateToken($account);
                //var_dump($_SESSION);
                echo "true";
            } else {
                echo "deactivate";
            }
            
        } else {
            echo "false";
        }

    }

    public function logout()
    {
        $this->security->deactivate();
        session_destroy();
        header("Location: http://" . $_SERVER['SERVER_NAME'] . "/");
    }

    public function token()
    {
        var_dump($this->security->acceptConnexion());
    }

    public function getDatasFromGET()
    {
        var_dump($this->inputGet());
    }

    public function getDatasFromPOST()
    {
        var_dump($this->inputPost());
    }

    public function getDatasFromPUT()
    {
        var_dump($this->inputPut());
    }

    public function delete()
    {
        var_dump($this->inputPut());
        var_dump($this->inputPost());
        var_dump($this->inputGet());
    }

    public function getByValue($value)
    {
        echo "valeur passée dans l'uri : " . $value;
    }

}