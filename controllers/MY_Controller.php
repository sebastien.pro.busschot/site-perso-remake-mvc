<?php
namespace BWB\Framework\mvc\controllers;
use BWB\Framework\mvc\Controller;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of ViewController
 *
 * @author loic
 */
class MY_Controller extends Controller{

    private $exceptions = ["/forgetPassword","/forgetPassword/send"];

    public function __construct(){
        parent::__construct();

        if(!in_array($_SERVER["REQUEST_URI"],$this->exceptions)){
            if(!$this->security->acceptConnexion()){
                header('Location: http://busschot.fr/');
            }
        }
    }

    /**
     * @params : string or array of authorised role
     */
    public function accesGuaranted($params){
        // params = role || roles
        $acces = false;
        if(is_array($params)){
            foreach ($params as $param) {
                $acces = $param == $_SESSION['role'] ? true : $acces;
            }
        } else if(is_string($params)) {
            $acces = $param == $_SESSION['role'] ? true : $acces;
        } else {
            var_dump("Erreur, accesGuaranted require a string or an array");
        }

        if(!$acces || !$this->security->acceptConnexion()){
            header('Location: http://busschot.fr/');
        }
    }
}