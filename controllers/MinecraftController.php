<?php
namespace BWB\Framework\mvc\controllers;
use BWB\Framework\mvc\controllers\MY_Controller;
use BWB\Framework\mvc\dao\DAOMinecraft;

session_start();


class MinecraftController extends MY_Controller{

    public function getView(){
        $this->accesGuaranted($users = ['user','admin']);

        $data = array(
            "title" => "Minecraft Statut"
        );

        //var_dump($data);
        $this->render("minecraft", $data);
    }

    public function getIpServer(){
        $this->accesGuaranted($users = ['user','admin']);
        $dao = new DAOMinecraft;
        $ip = $dao->retrieve(1)->getIp();
        echo json_encode([
            "ip" => $ip
        ]);
    }

    public function setIpServer(){
        //$this->accesGuaranted($users = ['user','admin']);
        $dao = new DAOMinecraft;
        //var_dump($this->inputPost()["ip"]);
        $dao->update($this->inputPost()["ip"]);
    }
}