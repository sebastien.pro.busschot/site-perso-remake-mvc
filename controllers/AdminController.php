<?php
namespace BWB\Framework\mvc\controllers;
use BWB\Framework\mvc\controllers\MY_Controller;
use BWB\Framework\mvc\dao\DAOAccount;
use BWB\Framework\mvc\dao\DAOPassword;
use BWB\Framework\mvc\dao\DAOStatusaccount;

session_start();


class AdminController extends MY_Controller{

    public function manageUsers(){
        $this->accesGuaranted($users = ['admin']);
        $daoAccount = new DAOAccount;
        $users = $daoAccount->getAll();
        //var_dump($users);
        $passwords = [];
        $status = [];
        $daoPassword = new DAOPassword;
        $daoStatus = new DAOStatusaccount;

        foreach ($users as $row) {
            //var_dump($row->getId_password());
            $id = $row->getId_password();
            $alias = $row->getAlias();
            $passwords[$alias] = $daoPassword->retrieve($id);
            $status[$alias] = $daoStatus->retrieve($row->getId());
        }
        //var_dump($status);



        $datas = array(
            "title" => "Gestion de utilisateurs",
            "users" => $users,
            "passwords" => $passwords,
            "status" => $status
         );
         $this->render("manageUsers", $datas);
    }

    public function updateUser(){
        $datas = $this->inputPost();
        $daoAccount = new DAOAccount;
        $daoPassword = new DAOPassword;
        $daoStatus = new DAOStatusaccount;

        $datas['account_id'] = $datas['id'];
        //var_dump($datas); 

        $accountEntity = $daoAccount->getEntity($datas);
        $statusEntity = $daoStatus->getEntity($datas);
        $bddPassword = $daoPassword->retrieve($datas['id_password']);
        
        
        if($bddPassword->getPassword() !== $datas['password']){
            $passwordEntity = $daoPassword->getEntity(array(
                "id" => $datas['id_password'],
                "password" => password_hash ( $datas['password'], PASSWORD_BCRYPT, ['cost'=>9])
            ));
            $daoPassword->update($passwordEntity);
        }
        
        $daoAccount->update($accountEntity);
        $daoStatus->update($statusEntity);
    }

    public function removeUser(){
        $id = $this->inputPut()['id'];
        $daoAccount = new DAOAccount;
        $daoPassword = new DAOPassword;
        $daoStatus = new DAOStatusaccount;

        $accountEntity = $daoAccount->retrieve($id);
        
        $daoAccount->delete($id);
        $daoStatus->delete($id);
        $daoPassword->delete($accountEntity->getId_password());
         
    }

    public function myAccount(){
        $daoAccount = new DAOAccount;
        $daoPassword = new DAOPassword;
        
        $accountEntity = $daoAccount->retrieve($_SESSION['id']);
        $passwordEntity = $daoPassword->retrieve($accountEntity->getId_password());
        
        $datas = array(
            "title" => "Mon compte",
            "accountEntity" => $accountEntity,
            "passwordEntity" => $passwordEntity
        );

        $this->render("myAccount", $datas);
    }

}