<?php
namespace BWB\Framework\mvc\dao;
use BWB\Framework\mvc\DAO;
use BWB\Framework\mvc\models\Statusaccount;

/* 
*creer avec l'objet issue de la classe CreateEntity Class 
*/


class DAOStatusaccount extends DAO {

	public function __construct(){
		parent::__construct();
	}

/* ____________________Crud methods____________________*/


	public function create ($entity){

		$sql = "INSERT INTO statusAccount (account_id,status,keyActivation) VALUES('" . $entity->getAccount_id() . "','" . $entity->getStatus() . "','".$entity->getKeyActivation()."')";
		//var_dump($sql);
		$this->getPdo()->query($sql);
	}


	public function retrieve ($id){
		$sql = "SELECT * FROM statusAccount WHERE account_id=" . $id;
		$statement = $this->getPdo()->query($sql);
		$result = $statement->fetch();
		$entity = new Statusaccount($result);
		return $entity;
	}


	public function update ($entity){

		$sql = "UPDATE statusAccount SET account_id = '" . $entity->getAccount_id() ."',status = '" . $entity->getStatus() ."' WHERE account_id = '". $entity->getAccount_id()."'";
		//echo $sql;
		if ($this->getPdo()->exec($sql) !== 0){
			echo "Updated status\n";
		} else {
			echo "Failed update status\n";
		}
	}


	public function delete ($id){

		$sql = "DELETE FROM statusAccount WHERE account_id = " . $id;
		echo $sql;
		$this->getPdo()->query($sql);
	}

/* ____________________Repository methods____________________*/


	public function getAll (){
		$sql = "SELECT * FROM statusAccount";
		$statement = $this->getPdo()->query($sql);
		$results = $statement->fetchAll();
		$entities = array();

		foreach($results as $result){
			$entity = new Statusaccount();
			$entity->setAccount_id($result['account_id']);
			$entity->setStatus($result['status']);
			array_push($entities,$entity);
		}
		return $entities;
	}


	public function getAllBy ($filter){
		$sql = "SELECT * FROM statusAccount";
		$i = 0;
		foreach($filter as $key => $value){
			if($i===0){
				$sql .= " WHERE ";
			} else {
				$sql .= " AND ";
			}
			$sql .= $key . " = '" . $value . "'";
			$i++;
		}
		$entities = array();
		//var_dump($sql);
		$statement = $this->getPdo()->query($sql);
		$results = $statement->fetchAll();
		//var_dump($results);
		foreach($results as $result){
			$entity = new Statusaccount($result);
			array_push($entities,$entity);
		}
		return $entities;
	}

	public function getEntity($array){
		return new Statusaccount($array);
	}
}