<?php
namespace BWB\Framework\mvc\dao;
use BWB\Framework\mvc\DAO;
use BWB\Framework\mvc\models\Password;

/* 
*creer avec l'objet issue de la classe CreateEntity Class 
*/


class DAOPassword extends DAO {

	public function __construct(){
		parent::__construct();
	}

/* ____________________Crud methods____________________*/


	public function create ($entity){

		$password = password_hash ( $entity->getPassword(), PASSWORD_BCRYPT, ['cost'=>9]);
		$sql = "INSERT INTO password (password) VALUES('" . $password . "')";
		$this->getPdo()->query($sql);
	}


	public function retrieve($id){

		$sql = "SELECT * FROM password WHERE id=" . $id;
		$statement = $this->getPdo()->query($sql);
		$result = $statement->fetch();
		$entity = new Password($result);
		//var_dump($entity);
		return $entity;
	}


	public function update ($entity){
		
		$sql = "UPDATE password SET password='" . $entity->getPassword() ."' WHERE id=". $entity->getId();
		if ($this->getPdo()->exec($sql) !== 0){
			// echo "Updated password\n";
		} else {
			echo "Failed update password\n";
		}
	}


	public function delete ($id){

		$sql = "DELETE FROM password WHERE id= " . $id;
		$this->getPdo()->query($sql);
	}

/* ____________________Repository methods____________________*/


	public function getAll (){
		$sql = "SELECT * FROM password";
		$statement = $this->getPdo()->query($sql);
		$results = $statement->fetchAll();
		$entities = array();

		foreach($results as $result){
			$entity = new Password(array());
			$entity->setId($result['id']);
			$entity->setPassword($result['password']);
			array_push($entities,$entity);
		}
		return $entities;
	}


	public function getAllBy ($filter){
		$sql = "SELECT * FROM password";
		$i = 0;
		foreach($filter as $key => $value){
			if($i===0){
				$sql .= " WHERE ";
			} else {
				$sql .= " AND ";
			}
			$sql .= $key . " = " . $value . "'";
			$i++;
		}
		$entities = array();
		$statement = $this->getPdo()->query($sql);
		$results = $statement->fetchAll();
		foreach($results as $result){
			$entity = new Password(array());
			$entity->setId($result['id']);
			$entity->setPassword($result['password']);
			array_push($entities,$entity);
		}
		return $entities;
	}

	public function getEntity($array){
		return new Password($array);
	}

	public function lastId(){
		return $this->getPdo()->lastInsertId();
	}
}