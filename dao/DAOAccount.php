<?php
namespace BWB\Framework\mvc\dao;
use BWB\Framework\mvc\DAO;
use BWB\Framework\mvc\models\Account;

/* 
*creer avec l'objet issue de la classe CreateEntity Class 
*/


class DAOAccount extends DAO {

	public function __construct(){
		parent::__construct();
	}

/* ____________________Crud methods____________________*/


	public function create ($entity){
		$sql = "INSERT INTO account (firstname,lastname,email,alias,birthdate,role,id_password) VALUES('".$entity->getFirstname()."','".$entity->getLastname()."','".$entity->getEmail()."','".$entity->getAlias()."','".$entity->getBirthdate()."','".$entity->getRole()."','".$entity->getId_password()."')";
		$this->getPdo()->query($sql);
	}


	public function retrieve ($id){

		$sql = "SELECT * FROM account WHERE id='" . $id . "'";
		$statement = $this->getPdo()->query($sql);
		$result = $statement->fetch();
		//var_dump($result);
		$entity = new Account($result);
		return $entity;
	}


	public function update ($entity){

		$sql = "UPDATE account SET firstname = '" . $entity->getFirstname() ."',lastname = '" . $entity->getLastname() ."',email = '" . $entity->getEmail() ."',alias = '" . $entity->getAlias() ."',birthdate = '" . $entity->getBirthdate() ."',role = '" . $entity->getRole() ."',id_password = '" . $entity->getId_password() ."' WHERE id = '". $entity->getId()."'";
		//echo $sql;
		if ($this->getPdo()->exec($sql) !== 0){
			echo "Updated account\n";
		} else {
			echo "Failed update account\n";
		}
	}


	public function delete ($id){

		$sql = "DELETE FROM account WHERE id= " . $id;
		$this->getPdo()->query($sql);
	}

/* ____________________Repository methods____________________*/


	public function getAll (){
		$sql = "SELECT * FROM account";
		$statement = $this->getPdo()->query($sql);
		$results = $statement->fetchAll();
		$entities = array();

		foreach($results as $result){
			$entity = new Account($result);
			array_push($entities,$entity);
		}
		return $entities;
	}


	public function getAllBy($filter){
		$sql = "SELECT * FROM account";
		$i = 0;
		foreach($filter as $key => $value){
			if($i===0){
				$sql .= " WHERE ";
			} else {
				$sql .= " AND ";
			}
			$sql .= $key . " = '" . $value . "'";
			$i++;
		}
		//var_dump($sql);
		$entities = array();
		$statement = $this->getPdo()->query($sql);
		$results = $statement->fetchAll();
		foreach($results as $result){
			$entity = new Account($result);
			array_push($entities,$entity);
		}
		return $entities;
	}

	public function getByAliasandPassxord($alias, $password){
		$sql = "SELECT ac.id, ac.firstname, ac.lastname, ac.email, ac.alias, ac.birthdate, ac.role
		FROM account as ac
		INNER JOIN password as pwd on ac.id_password=pwd.id
		WHERE ac.alias='".$alias."' AND pwd.password='".$password."'";
		$statement = $this->getPdo()->query($sql);
		$results = $statement->fetchAll();
		$entities = array();

		foreach($results as $result){
			$entity = new Account(array());
			$entity->setId($result['id']);
			$entity->setFirstname($result['firstname']);
			$entity->setLastname($result['lastname']);
			$entity->setEmail($result['email']);
			$entity->setAlias($result['alias']);
			$entity->setBirthdate($result['birthdate']);
			$entity->setRole($result['role']);
			array_push($entities,$entity);
		}
		return $entities;
	}

	public function getEntity($array){
		return new Account($array);
	}

	public function lastId(){
		return $this->getPdo()->lastInsertId();
	}

	public function countAccounts(){
		$sql = "SELECT COUNT(*) FROM account";
		$statement = $this->getPdo()->query($sql);
		return $statement->fetch();
	}

}